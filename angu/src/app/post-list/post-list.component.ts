import { Component, OnInit } from '@angular/core';
import { Post } from '../models/post-model';
import { PostService } from '../services/postServices';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit{

  //posts!:Post[];

  posts!:Observable<Post[]>;
  //private postssubject: BehaviorSubject<Post[]> = new BehaviorSubject(this.posts);

  constructor(private postsServices:PostService){}


  ngOnInit(): void {
    //this.fetchallposts();
    //this.posts = this.postsServices.getPosts();
    this.posts = this.postsServices.getPosts();
  }


/*   fetchallposts():void {
    this.postsServices.getPosts().subscribe(
      data => {
        this.posts = data;
      },
      error => {
        console.error('une erreur sest produite');
      });
  }

  getAllPosts(): Post[] {
    return this.posts;
  }*/


/*   likePostById(postid:number,action:'like'|'dislike'):void{
    const post = this.postById(postid);
    action === 'like' ? post.nblike++ : post.nblike--;

  }  */
}
