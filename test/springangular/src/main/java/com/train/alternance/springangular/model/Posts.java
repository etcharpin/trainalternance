package com.train.alternance.springangular.model;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.GenerationType;
import jakarta.persistence.GeneratedValue;
import lombok.Data;
import jakarta.persistence.Table;

@Data
@Entity
@Table(name = "posts")
public class Posts {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "nblike")
    private int nblike;

    @Column(name = "imageURL")
    private String imageURL;

    @Column(name = "appareil")
    private String appareil;

    @Column(name = "textbtn")
    private String textbtn;
}
