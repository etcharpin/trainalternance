import { Component, OnInit,Input } from '@angular/core';
import { Post } from '../models/post-model';
import { PostService } from '../services/postServices';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-postspage',
  templateUrl: './postspage.component.html',
  styleUrls: ['./postspage.component.scss']
})
export class PostspageComponent{

@Input() 
post!:Post;



 //@Input() post!:Observable<Post|undefined>;

 constructor(private postService:PostService,private route:Router) {}




 
  onViewPost(){
    console.log(this.post);
    if(this.post && this.post.id){
      console.log(this.post.id);
      this.route.navigateByUrl(`posts/${this.post.id}`);
    }
    
  }
}
