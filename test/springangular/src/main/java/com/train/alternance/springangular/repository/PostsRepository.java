package com.train.alternance.springangular.repository;

import com.train.alternance.springangular.model.Posts;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostsRepository extends CrudRepository<Posts, Long> {


}
