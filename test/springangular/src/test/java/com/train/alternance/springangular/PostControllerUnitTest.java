package com.train.alternance.springangular;


import com.train.alternance.springangular.controller.PostsController;
import com.train.alternance.springangular.service.PostsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;


@WebMvcTest(controllers = PostsController.class)
public class PostControllerUnitTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PostsService postsService;

    @Test
    public void testGetPosts() throws Exception{
        mockMvc.perform(get("/posts"))
                .andExpect(status().isOk());
    }
}
