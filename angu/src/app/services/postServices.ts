import {Injectable} from '@angular/core'
import { Post } from '../models/post-model'
import {HttpClient, HttpHeaders} from '@angular/common/http'
import { Observable, map } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class PostService {
  private apiURL = 'http://localhost:9000';

  constructor(private http:HttpClient){}



    getPosts():Observable<Post[]> {

      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*'
        })
      }
      console.log("getplu");
      try{
        return this.http.get<any>('http://localhost:9000/posts',httpOptions).pipe(map(datarray => datarray.map((data: any) => this.obsToPost(data))));
      } catch (error) {
        console.error('Une erreur de recuperation sest produite');
        throw error;
      }
      }

      private obsToPost(data:any):Post {
        return new Post(
          data.id,
          data.name,
          data.description,
          data.imageURL,
          data.nblike,
          "Like",
          "Iphone"
        )
      }
       postById(postid:number):Observable<Post|undefined>{

        const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
          })
        }

        try{
          return this.http.get<any>('http://localhost:9000/posts',httpOptions).pipe(map(po => po.find((pon: { id: number; }) => pon.id === postid)));
        } catch (error) {
          console.error('Une erreur de recuperation sest produite');
          throw error;
        }
      } 

/*       postById(postid:number):Observable<Post|undefined>{

        const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
          })
        }

        try{
          console.log(`http://localhost:9000/posts/${postid}`);
          return this.http.get<any>(`http://localhost:9000/posts/${postid}`,httpOptions);
        } catch (error) {
          console.error('Une erreur de recuperation sest produite');
          throw error;
        }
      }  */

      likePostById(postid:number,post:Post):void{
        const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
          })
        }
        if(post.textbtn === "Like"){
          post.textbtn = "Dislike";
          post.nblike++;
        }else{
          post.textbtn = "Like";
          post.nblike--;
        }

        try{
          this.http.put<Post|null>(`http://localhost:9000/posts/${postid}`,post,httpOptions);
        } catch (error) {
          console.error('Une erreur de recuperation sest produite');
          throw error;
        }

      }
}