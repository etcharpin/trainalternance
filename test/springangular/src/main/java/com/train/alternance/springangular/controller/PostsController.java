package com.train.alternance.springangular.controller;


import com.train.alternance.springangular.model.Posts;
import com.train.alternance.springangular.service.PostsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class PostsController {

    @Autowired
    private PostsService postsService;

    @PostMapping("/post")
    public Posts createPost(@RequestBody Posts posts){
        return postsService.savePosts(posts);
    }

    @GetMapping("/post/{id}")
    public Posts getPost(@PathVariable("id") final Long id ){
        Optional<Posts> posts = postsService.getPost(id);
        if (posts.isPresent()){
            return posts.get();
        }
        else{
            return null;
        }
    }


    @GetMapping("/posts")
    public Iterable<Posts> getPosts() {
        return postsService.getPosts();
    }

    @PutMapping("post/{id}")
    public Posts updatePost(@PathVariable("id") final Long id, @RequestBody Posts posts ){

        Optional<Posts> p = postsService.getPost(id);
        if (p.isPresent()){

            Posts currentPost = p.get();

            String name = posts.getName();
            if (name != null){
                currentPost.setName(name);
            }

            String description = posts.getDescription();
            if (description != null){
                currentPost.setDescription(description);
            }

            Integer nblike = posts.getNblike();
            if (nblike != null){
                currentPost.setNblike(nblike);
            }

            String imageURL = posts.getImageURL();
            if (imageURL != null){
                currentPost.setImageURL(imageURL);
            }

            String appareil = posts.getAppareil();
            currentPost.setAppareil(appareil);

            postsService.savePosts(currentPost);
            return currentPost;


        }
        else {
            return null;
        }
    }


    @DeleteMapping("/post/{id}")
    public void deletePost(@PathVariable("id") final Long id){
        postsService.deletePosts(id);
    }
}
