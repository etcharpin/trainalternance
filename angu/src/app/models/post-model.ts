export class Post {

/*     id!:number;
    name!:string;
    description!:string;
    imageURL!:string;
    nblike!:number;
    textbtn!:string;
    appareil?:string; */

    constructor( 
        public id:number,
        public name:String,
        public description:String,
        public imageURL:String,
        public nblike:number,
        public textbtn : String,
        public appareil : String
    ) {}
}