package com.train.alternance.springangular.service;


import com.train.alternance.springangular.model.Posts;
import com.train.alternance.springangular.repository.PostsRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Data
@Service
public class PostsService {

    @Autowired
    private PostsRepository postsRepository;

    public Optional<Posts> getPost(final Long id){
        return postsRepository.findById(id);
    }

    public Iterable<Posts> getPosts(){
        return postsRepository.findAll();
    }

    public void deletePosts(final Long id){
        postsRepository.deleteById(id);
    }

    public Posts savePosts(Posts posts){
        return postsRepository.save(posts);
    }
}
