import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-page-acceuil',
  templateUrl: './page-acceuil.component.html',
  styleUrls: ['./page-acceuil.component.scss']
})
export class PageAcceuilComponent {


  constructor(private router:Router){}

  onContinue(){
    this.router.navigateByUrl("posts");
  }
}
