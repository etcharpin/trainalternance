import { NgModule } from "@angular/core";
import { Routes, RouterModule} from "@angular/router";
import { PostListComponent } from "./post-list/post-list.component";
import { PageAcceuilComponent } from "./page-acceuil/page-acceuil.component";
import { SinglePostComponent } from "./single-post/single-post.component";

const routes : Routes = [

    {path: 'posts',component: PostListComponent},
    {path: '',component:PageAcceuilComponent},
    {path: 'posts/:id', component:SinglePostComponent}
];


    @NgModule({
        imports: [
            RouterModule.forRoot(routes)
        ],
        exports: [
            RouterModule
        ]
    })


    export class AppRoutingModule {}