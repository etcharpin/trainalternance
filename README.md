[![pipeline status](https://gitlab.isima.fr/etcharpin/trainalternance/badges/master/pipeline.svg)](https://gitlab.isima.fr/etcharpin/trainalternance/-/commits/master)


[![coverage report](https://gitlab.isima.fr/etcharpin/trainalternance/badges/master/coverage.svg)](https://gitlab.isima.fr/etcharpin/trainalternance/-/commits/master)


[![Latest Release](https://gitlab.isima.fr/etcharpin/trainalternance/-/badges/release.svg)](https://gitlab.isima.fr/etcharpin/trainalternance/-/releases)

Projet de test front angular back springboot pour entrainement alternance
