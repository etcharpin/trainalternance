CREATE TABLE IF NOT EXISTS posts (
                                     id INT AUTO_INCREMENT PRIMARY KEY,
                                     name VARCHAR(20) NOT NULL,
    description VARCHAR(250) NOT NULL ,
    nblike INT NOT NULL,
    imageURL VARCHAR(250) NOT NULL,
    appareil VARCHAR(100),
    textbtn VARCHAR(10) NOT NULL
    );

INSERT INTO posts (name, description, nblike,imageURL,appareil) VALUES
                                                  ('Laurent', 'Super photo',3,'https://cdn.pixabay.com/photo/2015/05/31/16/03/teddy-bear-792273_1280.jpg','Iphone','LIKE'),
                                                  ('Jerome', 'Super photo2',0,'https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/Three_Rock_Mountain_Southern_Tor.jpg/2880px-Three_Rock_Mountain_Southern_Tor.jpg','Samsung s10','LIKE'),
                                                  ('Geraldine', 'Super photo3',2,'https://wtop.com/wp-content/uploads/2020/06/HEALTHYFRESH.jpg',NULL,'DISLIKE'),
                                                  ('Bruno', 'Super photo4',12,'https://cdn.pixabay.com/photo/2015/05/31/16/03/teddy-bear-792273_1280.jpg',NULL,'LIKE'),
                                                  ('Karine', 'Super photo5',0,'https://wtop.com/wp-content/uploads/2020/06/HEALTHYFRESH.jpg','Nokia Lumia','DISLIKE');