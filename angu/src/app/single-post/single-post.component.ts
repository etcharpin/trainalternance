import { Component, Input, OnInit } from '@angular/core';
import { Post } from '../models/post-model';
import { PostService } from '../services/postServices';
import { ActivatedRoute } from '@angular/router';
import { PostListComponent } from '../post-list/post-list.component';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.scss']
})
export class SinglePostComponent implements OnInit{
  post$!:Observable<Post|undefined>;


  constructor(private postService:PostService,private route:ActivatedRoute) {}
  
  ngOnInit(): void {
    const postid = +this.route.snapshot.params['id'];
    this.post$ = this.postService.postById(postid);
    this.post$.subscribe(po => {
      if(po){
        console.log(po.id);
      }
    })
  }
  

  


   onLike(){

      this.post$.subscribe(post => {
        if(post){
          this.postService.likePostById(+this.route.snapshot.params['id'],post);
        }
      })

}
}
