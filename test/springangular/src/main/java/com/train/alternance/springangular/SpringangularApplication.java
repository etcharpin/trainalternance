package com.train.alternance.springangular;

import com.train.alternance.springangular.controller.PostsController;
import com.train.alternance.springangular.model.Posts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.stream.StreamSupport;

@CrossOrigin(origins = "*")
@SpringBootApplication
public class SpringangularApplication implements CommandLineRunner {

	@Autowired
	private PostsController postsController;

	public static void main(String[] args) {
		SpringApplication.run(SpringangularApplication.class, args);
	}

	@Override
	public void run(String ... args) throws Exception{

		Iterable<Posts> posts = postsController.getPosts();
		System.out.println("slt");
		System.out.println(StreamSupport.stream(posts.spliterator(),false).count());
		for (Posts po : posts){
			System.out.println(po);
		}


	}
}


